window.Project = (function (window, document, $, undefined) {
    'use strict';

    var app = {

        initialize: function () {
            console.log('working');
			app.initSliderCarousel('.active-slider');
            app.initSliderCarouselstyle2('.slider-fullwide');
            app.initSliderCarouselstyle3('.active-sliderstyle3');
        },
        initSliderCarousel : function($selector){
            $($selector).owlCarousel({
                items: 4,
                autoplay: false,
                loop: true,
                margin: 15,
                autoHeight:true,
                nav:true,
                dots:false,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:3,
                        nav:true
                    },
                    1000:{
                        items:4,
                        nav:true,
                        loop:true
                    }
                } 
            });
        }, // end initSliderCarousel
        
        initSliderCarouselstyle2 : function($selector){
            $($selector).owlCarousel({
                items: 1,
                autoplay: false,
                loop: true,
                dots:false,
                margin: 15,
                autoHeight:true,
                nav:true
            });
        },// end initSliderCarousel slider full width

        initSliderCarouselstyle3 : function($selector){
            $($selector).owlCarousel({
                items: 2,
                autoplay: false,
                loop: true,
                margin: 15,
                autoHeight:true,
                nav:true,
                dots:false,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:2,
                        nav:true
                    },
                    1000:{
                        items:2,
                        nav:true,
                        loop:true
                    }
                } 
            });
        }// end initSliderCarousel slider full width

    };



    $(document).ready(app.initialize);

    return app;

})(window, document, jQuery);
